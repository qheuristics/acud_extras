# acud-extras

acud-extras are software tools that complement the acud power market simulator. 
acud is a calculation engine that processes
input data provided as text files with a given format (YAML and csv) and delivers simulation
results as a (large) set of csv files. This places the burden of input and output data handling
on the user.

acud-extras is a collection of tools aimed at easing the user´s interaction with the acud
calculation engine by supporting typical modelling tasks such as:

- input data preparation - [acud configuration form](#markdown-header-acud-configuration-form)
- simulation execution management - [acud demo notebooks](#markdown-header-acud-demo-notebooks)
- solution data reporting and analysis - [acud dash apps](#markdown-header-acud-dash-apps)

## acud configuration form

Configuration files are a key ingredient of an acud input dataset. They can be easily created
and updated using the simplest of text editors. However, configuration files can also give rise 
to the following two complications:

- they must abide to the [YAML file format](http://yaml.org/) e.g. regarding indentation
- they involve a long and nested list of required and optional fields.

acud confimation form tackles these two complications. It is an Excel template that eases the preparation
of the configuration files. The user simply fills in Excel cells that are labeled and described
without needing to worry about proper indentation. The form includes all the available fields that may be
included in configuration files and provides visual clues on whether a field is required or not. 
Grey cells are *user-only* fields, that is, fields for which acud provides no default value
(e.g. file paths or model names). User-only fields with a continuous line border are required,
those with dotted borders are optional. White fields with dotted borders are *default-included* fields:
if the user does not complete the field, acud will apply a default value.

To install the acud configuration form on the user machine:

- download the file from [here](https://drive.google.com/open?id=1hppDse1sAXcnMXP-3ZGfTHFnk9LPWbb_)
- click on the Open command of the File menu to open the template
- save the file as an Excel template at C:\Users\Username\AppData\Roaming\Microsoft\Templates (this location
may vary slightly from one Excel version to another)

To use the template:

- click on the File menu New command: the acud configuration form template should now be
available to create a new workbook 
- complete the form fields as required and save the file __as a workbook__ at the 
corresponding case input folder (the directory where the case yaml configuration files should be stored);
this saved workbook we refer to below as a configuration workbook.

To instruct acud to use the workbook prepared with the acud configuration form template simply use
this workbook file instead of a run configuration file in the acud command calls, e.g. like this:

```
ad run <workbook.name.xlsx>
```

or

```
ad dry <workbook.name.xlsx>
```

This will trigger a preprocessing task at the start of the simulation that will automatically read
the field contents in the configuration workbook and write them into yaml configuration files that
will be used by the acud calculation engine thereafter. Note that any yaml files that you may have
in the configuration folder with the same names as those specified in the configuration workbook
will be overwritten without warning.

#### How to include point-and-click functionality to select file or folder in a configuration form workbook

1. Add this VBA macro for file selection

            Sub browseFilePath()
                On Error GoTo err
                Dim fileExplorer As FileDialog
                Set fileExplorer = Application.FileDialog(msoFileDialogFilePicker)

                'To allow or disable to multi select
                fileExplorer.AllowMultiSelect = False

                With fileExplorer
                    If .Show = -1 Then 'Any file is selected
                        ActiveCell.Value = .SelectedItems.Item(1)
                    Else ' else dialog is cancelled
                        MsgBox "You have cancelled the dialogue"
                        ActiveCell.Value = "" ' when cancelled set blank as file path.
                    End If
                End With
            err:
                Exit Sub
            End Sub

1. Add this macro for folder selection

            Sub browseFolderPath()
                On Error GoTo err
                Dim fileExplorer As FileDialog
                Set fileExplorer = Application.FileDialog(msoFileDialogFolderPicker)

                'To allow or disable to multi select
                fileExplorer.AllowMultiSelect = False

                With fileExplorer
                    If .Show = -1 Then 'Any folder is selected
                        ActiveCell.Value = .SelectedItems.Item(1)
                    Else ' else dialog is cancelled
                        MsgBox "You have cancelled the dialogue"
                        ActiveCell.Value = "" ' when cancelled set blank as file path.
                    End If
                End With
            err:
                Exit Sub
            End Sub

1. Select cell to fill in with file or folder location
1. Run the corresponding macro (tip: use a macro short-cut for convenience)    


## acud demo notebooks

Notebook documents (or “notebooks”, all lower case) are documents produced by the Jupyter Notebook App,
which contain both computer code (e.g. python) and rich text elements(paragraph, equations, figures, links, etc…).
Notebook documents are human-readable documents containing the analysis description and the results
(figures, tables, etc..) as well as executable documents which can be run to perform data analysis.

acud demo notebooks show how to launch the acud calculation and suggests ways of handling the
output data for solution analysis purposes. This demo notebooks are provided through the online 
[nbviewer service](https://nbviewer.jupyter.org/).

Jupyter Notebooks rendered through nbviewer allow users to review the full content of the notebooks 
through any modern web browser without requiring *any* software to be installed. The price you pay 
for this convenience is that notebooks in NBviewer are *read-only*, that is, you cannot rerun the code
contained in the notebook (but you can examine the results obtained during the last saved Jupyter Notebook run).

These nbviewer notebooks become read-write once downloaded onto a machine with both the Jupyter Notebook application
and acud fully installed. This way, the downloaded notebooks may serve as e.g. as acud simulation session templates.

Follow these links to reach the acud demo notebooks on nbviewer:

- [acud Python session](https://nbviewer.jupyter.org/gist/qheuristics/2fd1956fb8eacac9d7a07257a760fdbe?flush_cache=true)
- [acud console session](https://nbviewer.jupyter.org/gist/qheuristics/0a890b8bdf899c226690065042537196?flush_cache=true)

You can actually gain access to a live demo environment without any software installation on your machine
via [mybinder.org](mybinder.org). Fill in the three fields in the form at the home page of this free online service as follows:

- GitHub repository name or URL: *qheuristics*
- Git branch, tag, or commit: *ad102*
- Path to a notebook file: *acud_python_102_demo.ipynb*

and then click on the Launch button. This will create a custom computing environment that will enable you
to interactively use a Jupyter notebook through your web browser to explore acud solution reporting features
(note that mybinder.org is a beta development of a research pilot).


## acud dash apps

acud dash apps are early drafts of dashboard experiments providing acud data visualization. They are a small set of 
[Dash-based](https://dash.plot.ly/) web apps that illustrate how input and output data can be explored interacively.

A live demo can be found [here](https://acudash1.herokuapp.com).

